# Nexperia Alfresco UI

## Nexperia UI application

### Development server

0. Run `cd nexperia-documents-ui`
1. Run `npm run dev_watch` for a dev server.
1. Or run `ng serve`
2. Run JAVA project. 
3. Navigate to `http://localhost:8080`. The app will automatically rebuild if you change any of the source files.

### Build

- Run `cd nexperia-documents-ui`
- Run `ng build` to build the dev project. 
- The build artifacts will be stored in the `resources` directory. 
- Final cmd: `ng build --prod`
