package com.nexperia.documents.service.alfresco;

import com.nexperia.documents.model.*;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service("alfrescoService")
public class AlfrescoService {

    private final AlfrescoIntegrationService alfrescoIntegrationService;

    @Value("${app.alfresco.spider.url}")
    private String alfrescoSpiderUrl;

    @Value("${app.alfresco.tdm.url}")
    private String alfrescoTdmUrl;

    private final String DOWNLOAD_ASSET_URL_FORMAT = "https://assets.nexperia.com/documents/%s/%s";

    @Autowired
    public AlfrescoService(AlfrescoIntegrationService alfrescoIntegrationService) {
        this.alfrescoIntegrationService = alfrescoIntegrationService;
    }

    public String getTicket(Credentials credentials, String system) {
        if (StringUtils.isEmpty(system)) return null;
        String ticketResponse = null;
        if (system.equals(Constants.TDM)) {
            ticketResponse = alfrescoIntegrationService.getTdmTicket(credentials, String.class);
        } else if (system.equals(Constants.DPS)) {
            ticketResponse = alfrescoIntegrationService.getSpiderLibraryTicket(credentials, String.class);
        } else {
            return null;
        }

        return parseTicket(ticketResponse);
    }

    private static String parseTicket(String ticketXML) {
        return ticketXML.substring(ticketXML.indexOf("<ticket>") + "<ticket>".length(), ticketXML.indexOf("</ticket>"));
    }

    public List<NexperiaDocument> getSpiderLibraryDocuments(String nexperiaDocumentName, String alfTicket) {
        String documentsSearchUrl = alfrescoIntegrationService.buildSpiderSearchURL(nexperiaDocumentName, nexperiaDocumentName.equals("*"));
        System.out.println("Searching URL: " + documentsSearchUrl);
        Object[] spiderLibraryDocumentsObjects = alfrescoIntegrationService.get(documentsSearchUrl, Object[].class);
        List<NexperiaDocument> spiderLibraryDocuments = new ArrayList<>();
        int checkCount = 0;
        for (Object document : spiderLibraryDocumentsObjects) {
            Map<String, String> doc = (Map<String, String>) document;
            NexperiaDocument nexperiaDocument = new NexperiaDocument();
            nexperiaDocument.setPublicationId(doc.get(Constants.FILE_ID));
            nexperiaDocument.setName(doc.get(Constants.FILE_NAME));
            nexperiaDocument.setDescription(doc.get(Constants.DESCRIPTION));
            nexperiaDocument.setDescriptiveTitle(doc.get(Constants.DESCRIPTIVE_TITLE));
            nexperiaDocument.setDocumentType(doc.get(Constants.FILE_TYPE));
            nexperiaDocument.setVersion(doc.get(Constants.VERSION));
            nexperiaDocument.setNodeUUID(doc.get(Constants.NODE_UUID));
            nexperiaDocument.setTitle(doc.get(Constants.TITLE));
            nexperiaDocument.setSystem("SPIDER Library");
            nexperiaDocument.setPtw(doc.get(Constants.PTW).equals(Boolean.TRUE.toString()) ? "Yes" : "No");
            if (Constants.IMAGE_EXTENSIONS.contains(FilenameUtils.getExtension(doc.get(Constants.FILE_NAME)))) {
                String downloadAssetUrl = String.format(DOWNLOAD_ASSET_URL_FORMAT,
                        doc.get(Constants.FILE_TYPE).replace(" ", "-").toLowerCase(),
                        doc.get(Constants.FILE_NAME));
//                checkCount++;
//                if (checkAssetDownloadUrl(downloadAssetUrl, checkCount)) {
                    nexperiaDocument.setDownloadAssetUrl(downloadAssetUrl);
//                }
            }
            String dateStr = doc.get(Constants.RELEASE_DATE);
            nexperiaDocument.setReleaseDate(StringUtils.substringBefore(dateStr, "T"));
            Object magCodes = doc.get(Constants.MAG_CODES);
            if (magCodes != null) {
                nexperiaDocument.setMagCodes(((ArrayList) magCodes).stream().collect(Collectors.joining(",")).toString());
            }
            nexperiaDocument.setDownloadUrl(alfrescoSpiderUrl + String.format(Constants.DOWNLOAD_FORMAT, doc.get(Constants.NODE_UUID), doc.get(Constants.FILE_NAME), alfTicket));
            nexperiaDocument.setViewUrl(String.format(alfrescoSpiderUrl + Constants.DOWNLOAD_FORMAT, doc.get(Constants.NODE_UUID), doc.get(Constants.FILE_NAME), alfTicket).replace("/a/", "/d/"));
            spiderLibraryDocuments.add(nexperiaDocument);
        }
        return spiderLibraryDocuments;
    }

    /**
     * Checks if URL alive and real
     *
     * @param downloadAssetUrl - url to check
     * @param checkCount
     * @return true if there is content on provided url
     */
    private boolean checkAssetDownloadUrl(String downloadAssetUrl, int checkCount) {
        if (checkCount > 20) return true;
        HttpURLConnection huc = null;
        try {
            System.out.println("[ " + checkCount + " ] Checking '" + downloadAssetUrl + "' url...");
            URL url = new URL(downloadAssetUrl);
            huc = (HttpURLConnection) url.openConnection();
            huc.setConnectTimeout(2000);
            return huc.getResponseCode() == 200;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            huc.disconnect();
        }
        return false;
    }

    public List<NexperiaDocument> getTdmDocuments(String nexperiaDocumentName, List<String> magCodes, String alfTicket) {
        String documentsSearchUrl = alfrescoIntegrationService.buildTdmSearchURL(nexperiaDocumentName, magCodes);
        System.out.println("Searching URL: " + documentsSearchUrl);
        Object[] tdmDocumentsObjects = alfrescoIntegrationService.get(documentsSearchUrl, Object[].class);
        List<NexperiaDocument> tdmDocuments = new ArrayList<>();
        for (Object document : tdmDocumentsObjects) {
            Map<String, String> doc = (Map<String, String>) document;
            NexperiaDocument nexperiaDocument = new NexperiaDocument();
            nexperiaDocument.setPublicationId(doc.get(Constants.FILE_ID));
            nexperiaDocument.setName(doc.get(Constants.FILE_NAME));
            nexperiaDocument.setDescription(doc.get(Constants.DESCRIPTION));
            nexperiaDocument.setDescriptiveTitle(doc.get(Constants.DESCRIPTIVE_TITLE));
            nexperiaDocument.setDocumentType(doc.get(Constants.FILE_TYPE));
            nexperiaDocument.setVersion(doc.get(Constants.VERSION));
            nexperiaDocument.setNodeUUID(doc.get(Constants.NODE_UUID));
            nexperiaDocument.setTitle(doc.get(Constants.TITLE));
            nexperiaDocument.setSystem("TDM");
            nexperiaDocument.setPtw(doc.get(Constants.PTW).equals(Boolean.TRUE.toString()) ? "Yes" : "No");
            nexperiaDocument.setMagCodes(doc.get(Constants.MAG_CODES));
            nexperiaDocument.setAdditionalInfo(doc.get(Constants.ADDITIONAL_INFO));
            String dateStr = doc.get(Constants.RELEASE_DATE);
            nexperiaDocument.setReleaseDate(StringUtils.substringBefore(dateStr, "T"));
            nexperiaDocument.setDownloadUrl(alfrescoTdmUrl + String.format(Constants.TDM_WS_DOWNLOAD_FORMAT, doc.get(Constants.NODE_UUID)));
            tdmDocuments.add(nexperiaDocument);
        }
        return tdmDocuments;
    }

    public UserAuthDetails getUserDetails(String currentUsername, String alfTicket) {
        String userDetailsUrl = alfrescoIntegrationService.buildUserDetailsURL(currentUsername, alfTicket);
        return alfrescoIntegrationService.get(userDetailsUrl, UserAuthDetails.class);
    }

    public boolean isDataSheetCustomer(String currentUsername, String alfTicket) {
        String isDataSheetCustomerUrl = alfrescoIntegrationService.isDataSheetCustomerDetailsURL(currentUsername, alfTicket);
        Groups groups = alfrescoIntegrationService.get(isDataSheetCustomerUrl, Groups.class);
        if (groups.getGroups() != null && groups.getGroups().size() > 0) {
            final List<String> groupNames = groups.getGroups().stream().flatMap(m -> m.values().stream()).collect(Collectors.toList());
            return groupNames.contains(Constants.CS_DATASHEET_GROUP);
        }
        return false;
    }

    public List<String> DataSheetCustomerInformation(String currentUsername, String alfTicket) {
        String isDataSheetCustomerUrl = alfrescoIntegrationService.isDataSheetCustomerDetailsURL(currentUsername, alfTicket);
//        throw new RuntimeException("No such user");
        Groups groups = alfrescoIntegrationService.get(isDataSheetCustomerUrl, Groups.class);
        if (groups.getGroups() != null && groups.getGroups().size() > 0) {
            return groups.getGroups().stream().flatMap(m -> m.values().stream()).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    public List<String> getListOfMagCodeGroups(List<String> allGroups, String alfTicket) {
        List<String> resultList = new ArrayList<>(allGroups);
        String csDataSheetGroupsUrl = alfrescoIntegrationService.csDataSheetGroupsDetailsURL(alfTicket);
        CSGroups csGroups = alfrescoIntegrationService.get(csDataSheetGroupsUrl, CSGroups.class);
        List<String> csGShortNames = new ArrayList<>();
        if (csGroups.getData() != null && csGroups.getData().size() > 0) {
            csGShortNames = csGroups.getData().stream().map(map -> map.get("shortName")).collect(Collectors.toList());
        }
        resultList.retainAll(csGShortNames);
        return resultList;
    }

    public List<String> getListOfS5Groups(List<String> allGroups) {
        List<String> resultList = new ArrayList<>(allGroups);
        resultList.retainAll(Constants._S500_GROUPS);
        return resultList;
    }
}
