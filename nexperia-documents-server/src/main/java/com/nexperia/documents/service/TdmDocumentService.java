package com.nexperia.documents.service;

import com.nexperia.documents.service.alfresco.AlfrescoService;
import com.nexperia.documents.model.NexperiaDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TdmDocumentService  {

    private final AlfrescoService alfrescoService;

    @Autowired
    public TdmDocumentService(AlfrescoService alfrescoService) {
        this.alfrescoService = alfrescoService;
    }

    public List<NexperiaDocument> findNexperiaDocumentByName(String nexperiaDocumentName, List<String> magCodes, String alfTicket) {
        System.out.println("Searching for document in TDM: " + nexperiaDocumentName);
        return alfrescoService.getTdmDocuments(nexperiaDocumentName, magCodes, alfTicket);
    }

}
