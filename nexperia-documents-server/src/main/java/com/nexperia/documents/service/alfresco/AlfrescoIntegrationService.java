package com.nexperia.documents.service.alfresco;

import com.nexperia.documents.model.Credentials;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;

@Service
public class AlfrescoIntegrationService {
    private final RestTemplate restTemplate;

    private static final Map<String, String> URL_BAD_SYMBOLS_REPLACE = Map.of(
            "&", "%26",
            "#", "%23"
    );

    @Value("${app.alfresco.spider.url}")
    private String alfrescoSpiderUrl;

    @Value("${app.alfresco.tdm.url}")
    private String alfrescoTdmUrl;

    private static final String ENDPOINT_APP = "/s";

    public AlfrescoIntegrationService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public <T> T get(String url, Class<T> type) {
        return restTemplate.getForEntity(url, type).getBody();
    }

    public String buildSpiderSearchURL(String documentName, boolean isAll) {
        StringBuilder url = new StringBuilder()
                .append(alfrescoSpiderUrl)
                .append(ENDPOINT_APP)
                .append("/nxp/spc/search?properties=FildID,Title,FileType,FileName,Version,Description,DescriptiveTitle,NodeUUID,PublishedOnWeb,MAGCodes,ReleaseDate");
        if (!isAll) {
            url.append("&FileName=").append(documentName);
        }
        return url.toString();
    }

    public String buildTdmSearchURL(String documentName, List<String> magCodes) {
        StringBuilder url = new StringBuilder()
                .append(alfrescoTdmUrl)
                .append(ENDPOINT_APP)
                .append("/nexperia/tdm/search?documentType=Data sheet")
                .append("&documentName=*")
                .append(documentName);
        if (magCodes != null && magCodes.size() > 0) {
            url.append("&magCodes=")
                    .append(StringUtils.join(magCodes, ","));
        }
        return url.toString();
    }

    public String buildUserDetailsURL(String userName, String ticket) {
        StringBuilder url = new StringBuilder()
                .append(alfrescoSpiderUrl)
                .append(ENDPOINT_APP)
                .append("/api/people/")
                .append(userName)
                .append("?alf_ticket=")
                .append(ticket);
        return url.toString();
    }

    public String isDataSheetCustomerDetailsURL(String userName, String ticket) {
        StringBuilder url = new StringBuilder()
                .append(alfrescoTdmUrl)
                .append(ENDPOINT_APP)
                .append("/api/people/")
                .append(userName)
                .append("?groups=true")
                .append("&alf_ticket=")
                .append(ticket);
        return url.toString();
    }

    public String csDataSheetGroupsDetailsURL(String ticket) {
        return alfrescoTdmUrl +
                ENDPOINT_APP +
                "/api/groups/CS_DataSheet_Users/children?authorityType=GROUP" +
                "&alf_ticket=" +
                ticket;
    }

    public <T> T getSpiderLibraryTicket(Credentials credentials, Class<T> type) {
        return restTemplate.getForEntity(buildAlfTicketURL(alfrescoSpiderUrl, credentials), type).getBody();
    }

    public <T> T getTdmTicket(Credentials credentials, Class<T> type) {
        return restTemplate.getForEntity(buildAlfTicketURL(alfrescoTdmUrl, credentials), type).getBody();
    }

    private String buildAlfTicketURL(String serverUrl, Credentials credentials) {
        StringBuilder url = new StringBuilder()
                .append(serverUrl)
                .append(ENDPOINT_APP)
                .append("/api/login?")
                .append("u=")
                .append(credentials.getUsername())
                .append("&pw=")
                .append(credentials.getPassword());
        return url.toString();
    }
}
