package com.nexperia.documents.service;

import com.nexperia.documents.model.Constants;
import com.nexperia.documents.model.NexperiaDocument;
import com.nexperia.documents.security.ApplicationUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.saml2.provider.service.authentication.Saml2Authentication;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DocumentsService {

    public final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final SpiderLibraryDocumentService spiderLibraryDocumentService;
    private final TdmDocumentService tdmDocumentService;
    private final AwsS3Service awsS3Service;

    public DocumentsService(SpiderLibraryDocumentService spiderLibraryDocumentService, TdmDocumentService tdmDocumentService, AwsS3Service awsS3Service) {
        this.spiderLibraryDocumentService = spiderLibraryDocumentService;
        this.tdmDocumentService = tdmDocumentService;
        this.awsS3Service = awsS3Service;
    }

    public List<NexperiaDocument> findDocumentsBySearchString(String searchString) {
        List<NexperiaDocument> resultListOfDocuments = new ArrayList<NexperiaDocument>();
        if (searchString.split(",").length > 1) {
            Arrays.stream(searchString.split(","))
                    .collect(Collectors.toList())
                    .forEach(documentNameToSearch -> resultListOfDocuments.addAll(findDocumentsByName(documentNameToSearch)));
        } else {
            resultListOfDocuments.addAll(findDocumentsByName(searchString));
        }
        return resultListOfDocuments;
    }

    public List<NexperiaDocument> findDocumentsByName(String documentName) {
        final String originSearchDocumentName = documentName.trim().replace("*", "");
        documentName = originSearchDocumentName;

        final Saml2Authentication authentication = (Saml2Authentication) SecurityContextHolder.getContext().getAuthentication();
        final ApplicationUser uDetails = (ApplicationUser) authentication.getDetails();
        String dpsAlfTicket = uDetails.getDpsAlfTicket();
        String cpsAlfTicket = uDetails.getCpsAlfTicket();
        boolean isCSDataSheetUser = uDetails.isCSDataSheetUser();
        boolean isS5Customer = uDetails.isS5Customer();
        logger.info("Search request: {} | {} | isCSDataSheetUser = {} | isS5Customer = {}", documentName, uDetails.getUsername(), isCSDataSheetUser, isS5Customer);
        logger.debug("isCSDataSheetUser = '{}', isS5Customer = '{}'", isCSDataSheetUser, isS5Customer);
        List<NexperiaDocument> resultListOfDocuments = new ArrayList<NexperiaDocument>();
        List<NexperiaDocument> spiderLibraryDocuments = spiderLibraryDocumentService.findNexperiaDocumentByName(documentName.replace("-", ""), dpsAlfTicket);
        List<NexperiaDocument> awsDocuments = awsS3Service.findDocuments(documentName);
        resultListOfDocuments.addAll(spiderLibraryDocuments);
        resultListOfDocuments.addAll(awsDocuments);
        if (isCSDataSheetUser) {
            List<String> magCodes = uDetails.getMagCodes();
            List<NexperiaDocument> tdmDocuments = tdmDocumentService.findNexperiaDocumentByName(documentName, magCodes, cpsAlfTicket);
            resultListOfDocuments.addAll(tdmDocuments);
        }
        //Remove all _S5* docements from the result
        ArrayList<NexperiaDocument> allS5Documents = new ArrayList<>();
        List<String> sGroups = uDetails.getS5CustomerGroups();
        //Get all S5* documents from the result
        resultListOfDocuments.forEach(doc -> {
            Constants._S500_GROUPS.forEach(sGroup -> {
                if (doc.getName().contains(sGroup)) {
                    allS5Documents.add(doc);
                }
            });
        });
        //Remove all _S5* documents from the result
        resultListOfDocuments.removeAll(allS5Documents);
        if (isS5Customer) {
            //Select only allowed documents
            List<NexperiaDocument> allowedDocuments = new ArrayList<>();
            allS5Documents.forEach(doc -> {
                sGroups.forEach(sGroup -> {
                    if (doc.getName().contains(sGroup)) {
                        allowedDocuments.add(doc);
                    }
                });
            });
            //Add allowed documents to the result
            resultListOfDocuments.addAll(allowedDocuments);
        }

        //General cleanup if _ is the first symbol, for example _S*
        resultListOfDocuments = resultListOfDocuments.stream()
                .filter(nexperiaDocument -> nexperiaDocument.getName().contains(originSearchDocumentName))
                .collect(Collectors.toList());

        return resultListOfDocuments;
    }

    public File downloadNexperiaDocumentFromAWSByName(String path, String documentName) {
        final Saml2Authentication authentication = (Saml2Authentication) SecurityContextHolder.getContext().getAuthentication();
        final ApplicationUser uDetails = (ApplicationUser) authentication.getDetails();
        boolean isCSDataSheetUser = uDetails.isCSDataSheetUser();
        boolean isS5Customer = uDetails.isS5Customer();
        logger.info("Download request: {} | {} | isCSDataSheetUser = {} | isS5Customer = {}", documentName, uDetails.getUsername(), isCSDataSheetUser, isS5Customer);

        return awsS3Service.download(path, documentName);
    }

    public File veiwAWSDocument(String path, String documentName) {
        final Saml2Authentication authentication = (Saml2Authentication) SecurityContextHolder.getContext().getAuthentication();
        final ApplicationUser uDetails = (ApplicationUser) authentication.getDetails();
        boolean isCSDataSheetUser = uDetails.isCSDataSheetUser();
        boolean isS5Customer = uDetails.isS5Customer();
        logger.info("View request: {} | {} | isCSDataSheetUser = {} | isS5Customer = {}", documentName, uDetails.getUsername(), isCSDataSheetUser, isS5Customer);

        return awsS3Service.download(path, documentName);
    }

}
