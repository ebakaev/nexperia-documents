package com.nexperia.documents.service;

import com.nexperia.S3Client;
import com.nexperia.documents.model.NexperiaDocument;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class AwsS3Service {

    protected final Log logger = LogFactory.getLog(this.getClass());

    private S3Client s3Client;

    @Value("${app.aws.s3.bucketName}")
    String bucketName;

    @Value("${app.aws.temp.folder}")
    String tempFolder;

    @Value("${app.aws.files.path}")
    String rootPath;

    public AwsS3Service(@Value("${app.aws.s3.accessKeyId}") String accessKey,
                        @Value("${app.aws.s3.secretKeyId}") String secretKey,
                        @Value("${app.aws.s3.bucketName}") String bucketName) {
        this.s3Client = new S3Client();
        this.s3Client.setAccessKeyValue(accessKey);
        this.s3Client.setSecretKeyValue(secretKey);
        this.s3Client.setRootBucketName(bucketName);
    }

    public List<NexperiaDocument> findDocuments(String searchString) {
        logger.debug("Searching in AWS S3 for: " + searchString);
        List<NexperiaDocument> result = new ArrayList<>();

        try {
            Map<String, Map<String, String>> documentsInAWSByName = s3Client.findDocumentsInAWSByName(bucketName, rootPath, searchString);
            final String baseUrl = ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString();
            logger.debug("Base URL: " + baseUrl);

            documentsInAWSByName.keySet().forEach(documentKey -> {
                Map<String, String> properties = documentsInAWSByName.get(documentKey);
                String documentName = documentKey.substring(documentKey.lastIndexOf("/") + 1);
                System.out.println("\nFound by search string in AWS: " + documentName);
                NexperiaDocument document = new NexperiaDocument();
                document.setName(documentName);
                document.setSystem("AWS");
                document.setDocumentType(properties.get(S3Client.PROP_DRAWING_S3_DOCUMENT_TYPE));
                document.setMagCodes(properties.get(S3Client.PROP_DRAWING_S3_MAG_CODE));
                document.setDescriptiveTitle(properties.get(S3Client.PROP_DRAWING_S3_DESCRIPTIVE_TITLE));
                document.setPtw(properties.get(S3Client.PROP_DRAWING_S3_PTW));
                document.setReleaseDate(properties.get(S3Client.PROP_DRAWING_S3_CREATION_DATE));
                document.setPublicationId(properties.get(S3Client.PROP_DRAWING_S3_PUBLICATION_ID));
                document.setAwsKeyName(documentKey);
                document.setDownloadUrl(baseUrl + "/api/document/download/" + documentKey.replace(rootPath, ""));
                document.setViewUrl(baseUrl + "/api/document/view/" + documentKey.replace(rootPath, ""));
                result.add(document);
            });

            logger.debug("Found " + result.size() + " documents");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public File download(String path, String documentName) {
        File awsDocument = new File(tempFolder + documentName.substring(documentName.lastIndexOf("/") + 1));
        s3Client.downloadResource(bucketName, this.rootPath + path, documentName, awsDocument);
        return awsDocument;
    }

}
