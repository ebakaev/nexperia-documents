package com.nexperia.documents.service;

import com.nexperia.documents.service.alfresco.AlfrescoService;
import com.nexperia.documents.model.NexperiaDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SpiderLibraryDocumentService {

    private final AlfrescoService alfrescoService;

    @Autowired
    public SpiderLibraryDocumentService(AlfrescoService alfrescoService) {
        this.alfrescoService = alfrescoService;
    }

    public List<NexperiaDocument> findNexperiaDocumentByName(String nexperiaDocumentName, String alfTicket) {
        System.out.println("Searching for document in SPIDER Library: " + nexperiaDocumentName);
        return alfrescoService.getSpiderLibraryDocuments(nexperiaDocumentName, alfTicket);
    }

}
