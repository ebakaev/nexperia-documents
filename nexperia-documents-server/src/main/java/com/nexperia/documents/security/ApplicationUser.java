package com.nexperia.documents.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;
import java.util.List;

public class ApplicationUser extends User {

    private String dpsAlfTicket;
    private String cpsAlfTicket;
    private Boolean isCSDataSheetUser;
    private Boolean isS5Customer;
    private List<String> magCodes;
    private List<String> s5CustomerGroups;

    public ApplicationUser(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

    public ApplicationUser(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
    }

    public String getDpsAlfTicket() {
        return dpsAlfTicket;
    }

    public void setDpsAlfTicket(String dpsAlfTicket) {
        this.dpsAlfTicket = dpsAlfTicket;
    }

    public String getCpsAlfTicket() {
        return cpsAlfTicket;
    }

    public void setCpsAlfTicket(String cpsAlfTicket) {
        this.cpsAlfTicket = cpsAlfTicket;
    }

    public Boolean isCSDataSheetUser() {
        return isCSDataSheetUser;
    }

    public void setCSDataSheetUser(Boolean CSDataSheetUser) {
        isCSDataSheetUser = CSDataSheetUser;
    }

    public List<String> getMagCodes() {
        return magCodes;
    }

    public void setMagCodes(List<String> magCodes) {
        this.magCodes = magCodes;
    }

    public Boolean isS5Customer() {
        return isS5Customer;
    }

    public void setS5Customer(Boolean s5Customer) {
        isS5Customer = s5Customer;
    }

    public List<String> getS5CustomerGroups() {
        return s5CustomerGroups;
    }

    public void setS5CustomerGroups(List<String> s5CustomerGroups) {
        this.s5CustomerGroups = s5CustomerGroups;
    }
}
