package com.nexperia.documents.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CommonController {

    @GetMapping("/errorPage")
    public String error() {
        return "<p style=\"text-align:center\">Please contact Eduard Bakaev (ebakaev@evelopers.com) to investigate this issue. <a href=\"/\">Go to home page</a></p>";
    }

    @GetMapping("/homePage")
    public String homePage() {
        return "<p style=\"text-align:center\">Something went wrong, <a href=\"/\">Go to home page</a></p>";
    }

    @GetMapping("/welcomePage")
    public String welcome() {
        return "<!DOCTYPE html>\n" +
                "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
                "<head>\n" +
                "    <title>Welcome to Documents page</title>\n" +
                "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n" +
                "</head>\n" +
                "<body>\n" +
                "<p style=\"text-align:center\">Please contact Herman Elenbaas (herman.elenbaas@nexperia.com), Maarten de Boer (maarten.de.boer@nexperia.com) or Eduard Bakaev (ebakaev@evelopers.com) to get permissions on this Application!</p>\n" +
                "</body>\n" +
                "</html>";
    }
}
