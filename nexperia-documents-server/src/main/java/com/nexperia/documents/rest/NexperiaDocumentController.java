package com.nexperia.documents.rest;

import com.nexperia.documents.model.NexperiaDocument;
import com.nexperia.documents.model.UserAuthDetails;
import com.nexperia.documents.service.DocumentsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.saml2.provider.service.authentication.DefaultSaml2AuthenticatedPrincipal;
import org.springframework.security.saml2.provider.service.authentication.Saml2Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@RestController
@RequestMapping("/api")
public class NexperiaDocumentController {

    public final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    DocumentsService documentsService;

    @GetMapping("/document/search/{documentName}")
    public ResponseEntity<List<NexperiaDocument>> getNexperiaDocumentById(@PathVariable("documentName") String documentName) {
        return new ResponseEntity<>(documentsService.findDocumentsBySearchString(documentName.trim()), HttpStatus.OK);
    }

    @GetMapping("/document/download/{path}/{documentName}")
    public ResponseEntity<Resource> downloadNexperiaDocumentFromAWSByName(@PathVariable("path") String path,
                                                                          @PathVariable("documentName") String documentName) {
        File awsDocument = documentsService.downloadNexperiaDocumentFromAWSByName(path + "/", documentName.trim());

        HttpHeaders header = new HttpHeaders();
        header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + awsDocument.getName());
        header.add("Cache-Control", "no-cache, no-store, must-revalidate");
        header.add("Pragma", "no-cache");
        header.add("Expires", "0");

        Path fileLocation = Paths.get(awsDocument.getAbsolutePath());
        ByteArrayResource resource = null;
        try {
            resource = new ByteArrayResource(Files.readAllBytes(fileLocation));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return ResponseEntity.ok()
                .headers(header)
                .contentLength(awsDocument.length())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
    }

    @GetMapping(value = "/document/view/{path}/{documentName}")
    public ResponseEntity<InputStreamResource> veiwAWSDocument(@PathVariable("path") String path,
                                                               @PathVariable("documentName") String documentName) throws IOException {
        File awsDocument = documentsService.veiwAWSDocument(path + "/", documentName);
        InputStream in = new FileInputStream(awsDocument);

        return ResponseEntity.ok()
                .contentType(MediaType.IMAGE_JPEG)
                .body(new InputStreamResource(in));
    }

    @GetMapping(value = "/currentUser")
    public ResponseEntity<UserAuthDetails> getUserDetails() throws IOException {
        final Saml2Authentication authentication = (Saml2Authentication) SecurityContextHolder.getContext().getAuthentication();
        UserAuthDetails user = new UserAuthDetails();
        user.setUserName(authentication.getName());
        user.setEmail(authentication.getName());
        String displayName = (String) ((DefaultSaml2AuthenticatedPrincipal) authentication.getPrincipal()).getAttributes().get("http://schemas.microsoft.com/identity/claims/displayname").get(0);
        user.setName(displayName);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

}
