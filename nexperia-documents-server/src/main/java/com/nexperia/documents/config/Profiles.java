package com.nexperia.documents.config;

public class Profiles {
    public static final String
            //value should be a part of then name of application-{value}.properties file
            OKTA = "local",
            AZURE_QA = "qa",
            AZURE_PROD = "prod";
}
