package com.nexperia.documents.config;

import org.apache.commons.io.IOUtils;
import org.opensaml.security.x509.X509Support;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.saml2.core.Saml2X509Credential;
import org.springframework.security.saml2.provider.service.registration.InMemoryRelyingPartyRegistrationRepository;
import org.springframework.security.saml2.provider.service.registration.RelyingPartyRegistration;
import org.springframework.security.saml2.provider.service.registration.RelyingPartyRegistrationRepository;

import java.security.cert.X509Certificate;

@Configuration
public class RegistrationRepositoryConfigurator {
    @Value("${app.sertificate.file.path}")
    private String certificatePath;

//    @Value("${cert.signon.location}")
//    private String signonLocation;

    @Bean
    @Profile(Profiles.AZURE_PROD)
    protected RelyingPartyRegistrationRepository relyingPartyRegistrationsProd() throws Exception {
        X509Certificate certificate = X509Support.decodeCertificate(IOUtils.toByteArray(getClass().getClassLoader().getResourceAsStream(certificatePath)));
        Saml2X509Credential credential = Saml2X509Credential.verification(certificate);
        RelyingPartyRegistration registration = RelyingPartyRegistration
                .withRegistrationId("nexperia-documents-saml2")
                .entityId("https://www.documents-dam.pim.nexperia.com/saml2/service-provider-metadata/nexperia-documents-saml2")
                .assertionConsumerServiceLocation("https://www.documents-dam.pim.nexperia.com/login/saml2/sso/nexperia-documents-saml2")
                .assertingPartyDetails(party -> party
                        .entityId("https://sts.windows.net/59f00634-f68c-45dc-bf46-fcf62df94ffe/")
                        .singleSignOnServiceLocation("https://login.microsoftonline.com/59f00634-f68c-45dc-bf46-fcf62df94ffe/saml2")
                        .wantAuthnRequestsSigned(false)
                        .verificationX509Credentials(c -> c.add(credential))

                ).build();
        return new InMemoryRelyingPartyRegistrationRepository(registration);
    }

    @Bean
    @Profile(Profiles.AZURE_QA)
    protected RelyingPartyRegistrationRepository relyingPartyRegistrationsQa() throws Exception {
        X509Certificate certificate = X509Support.decodeCertificate(IOUtils.toByteArray(getClass().getClassLoader().getResourceAsStream(certificatePath)));
        Saml2X509Credential credential = Saml2X509Credential.verification(certificate);
        RelyingPartyRegistration registration = RelyingPartyRegistration
                .withRegistrationId("nexperia-documents-saml2")
                .entityId("https://qa.documents-dam.pim.nexperia.com/saml2/service-provider-metadata/nexperia-documents-saml2")
                .assertionConsumerServiceLocation("https://qa.documents-dam.pim.nexperia.com/login/saml2/sso/nexperia-documents-saml2")
                .assertingPartyDetails(party -> party
                        .entityId("https://sts.windows.net/59f00634-f68c-45dc-bf46-fcf62df94ffe/")
                        .singleSignOnServiceLocation("https://login.microsoftonline.com/59f00634-f68c-45dc-bf46-fcf62df94ffe/saml2")
                        .wantAuthnRequestsSigned(false)
                        .verificationX509Credentials(c -> c.add(credential))

                ).build();
        return new InMemoryRelyingPartyRegistrationRepository(registration);
    }

    @Bean
    @Profile(Profiles.OKTA)
    protected RelyingPartyRegistrationRepository relyingPartyRegistrations() throws Exception {
//        ClassLoader classLoader = getClass().getClassLoader();
//        File verificationKey = new File(classLoader.getResource("saml-certificate/okta.crt").getFile());
//        File verificationKey = new File("/appl/documents/saml-certificate/azure.crt");
//        X509Certificate certificate = X509Support.decodeCertificate(IOUtils.toByteArray(getClass().getClassLoader().getResourceAsStream("saml-certificate/azure.crt")));
        X509Certificate certificate = X509Support.decodeCertificate(IOUtils.toByteArray(getClass().getClassLoader().getResourceAsStream(certificatePath)));
        Saml2X509Credential credential = Saml2X509Credential.verification(certificate);
        RelyingPartyRegistration registration = RelyingPartyRegistration
                .withRegistrationId("nexperia-documents-saml2")
//                .entityId("https://qa.documents-dam.pim.nexperia.com/saml2/service-provider-metadata/nexperia-documents-saml2")
//                .assertionConsumerServiceLocation("https://qa.documents-dam.pim.nexperia.com/login/saml2/sso/nexperia-documents-saml2")
                .assertingPartyDetails(party -> party
                                .entityId("http://www.okta.com/exk62jplikBdB4hG75d7")
//                                .entityId("https://sts.windows.net/59f00634-f68c-45dc-bf46-fcf62df94ffe/")
                                .singleSignOnServiceLocation("https://dev-12071135.okta.com/app/dev-12071135_nexperiadocumnetsapp_1/exk62jplikBdB4hG75d7/sso/saml")
//                                .singleSignOnServiceLocation("https://login.microsoftonline.com/59f00634-f68c-45dc-bf46-fcf62df94ffe/saml2")
                                .wantAuthnRequestsSigned(false)
                                .verificationX509Credentials(c -> c.add(credential))

                ).build();
        return new InMemoryRelyingPartyRegistrationRepository(registration);
    }


}
