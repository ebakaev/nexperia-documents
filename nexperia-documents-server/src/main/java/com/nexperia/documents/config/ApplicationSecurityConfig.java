package com.nexperia.documents.config;

import com.nexperia.documents.filter.AlfrescoSecurityFilter;
import com.nexperia.documents.filter.UrlRedirectFilter;
import com.nexperia.documents.service.alfresco.AlfrescoService;
import org.apache.commons.io.IOUtils;
import org.opensaml.security.x509.X509Support;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.saml2.core.Saml2X509Credential;
import org.springframework.security.saml2.provider.service.metadata.OpenSamlMetadataResolver;
import org.springframework.security.saml2.provider.service.registration.InMemoryRelyingPartyRegistrationRepository;
import org.springframework.security.saml2.provider.service.registration.RelyingPartyRegistration;
import org.springframework.security.saml2.provider.service.registration.RelyingPartyRegistrationRepository;
import org.springframework.security.saml2.provider.service.servlet.filter.Saml2WebSsoAuthenticationFilter;
import org.springframework.security.saml2.provider.service.web.DefaultRelyingPartyRegistrationResolver;
import org.springframework.security.saml2.provider.service.web.Saml2MetadataFilter;

import javax.servlet.http.HttpServletRequest;
import java.security.cert.X509Certificate;

@Configuration
@EnableWebSecurity
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private RelyingPartyRegistrationRepository relyingPartyRegistrationRepository;

    private final AlfrescoService alfrescoService;

    @Autowired
    public ApplicationSecurityConfig(AlfrescoService alfrescoService) {
        this.alfrescoService = alfrescoService;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests(authorize ->
                        authorize
                                .antMatchers("/home").permitAll()
                                .antMatchers("/error").permitAll()
                                .antMatchers("/welcome").permitAll()
                                .anyRequest()
                                .authenticated()
                ).saml2Login();

        http.cors().disable();
        http.csrf().disable();

//        add auto -generation of ServiceProvider Metadata
        Converter<HttpServletRequest, RelyingPartyRegistration> relyingPartyRegistrationResolver = new DefaultRelyingPartyRegistrationResolver(relyingPartyRegistrationRepository);
        Saml2MetadataFilter filter = new Saml2MetadataFilter(relyingPartyRegistrationResolver, new OpenSamlMetadataResolver());
        AlfrescoSecurityFilter alfrescoSecurityFilter = new AlfrescoSecurityFilter(alfrescoService);
        UrlRedirectFilter redirectFilter = new UrlRedirectFilter();
        http.addFilterBefore(filter, Saml2WebSsoAuthenticationFilter.class);
        http.addFilterAfter(alfrescoSecurityFilter, Saml2WebSsoAuthenticationFilter.class);
        http.addFilterAfter(redirectFilter, AlfrescoSecurityFilter.class);
    }

}
