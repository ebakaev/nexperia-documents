package com.nexperia.documents.filter;

import com.nexperia.documents.model.Constants;
import com.nexperia.documents.model.Credentials;
import com.nexperia.documents.security.ApplicationUser;
import com.nexperia.documents.service.alfresco.AlfrescoService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.saml2.provider.service.authentication.Saml2Authentication;

import javax.servlet.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AlfrescoSecurityFilter implements Filter {

    protected final Log logger = LogFactory.getLog(this.getClass());

    private final AlfrescoService alfrescoService;

    public AlfrescoSecurityFilter(AlfrescoService alfrescoService) {
        this.alfrescoService = alfrescoService;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null /*&& authentication.getDetails() == null*/) {
            //add details to security context: Alfresco tickets for TDM, DPS.
            String username = "admin";
            String pwd = "Nex@dmin";
            String dpsTicket = alfrescoService.getTicket(new Credentials(username, pwd), Constants.DPS);
            String cpsTicket = alfrescoService.getTicket(new Credentials(username, pwd), Constants.TDM);
            List<String> csDataSheetInfo = new ArrayList<>();
            try {
                csDataSheetInfo = alfrescoService.DataSheetCustomerInformation(authentication.getName(), cpsTicket);
            } catch (Exception e) {
                RequestDispatcher dispatcher = servletRequest.getRequestDispatcher("/welcomePage");
                dispatcher.forward(servletRequest, servletResponse);
                return;
            }

            Boolean isCSDataSheetUser = csDataSheetInfo.contains(Constants.CS_DATASHEET_GROUP);
            Boolean isS5Customer = csDataSheetInfo.contains(Constants.C5_CUSTOMERS_GROUP);

            ApplicationUser user = new ApplicationUser(authentication.getName(), "pwd", new ArrayList<>());
            user.setDpsAlfTicket(dpsTicket);
            user.setCpsAlfTicket(cpsTicket);
            user.setCSDataSheetUser(isCSDataSheetUser);
            user.setS5Customer(isS5Customer);
            if (isCSDataSheetUser) {
                final List<String> listOfMagCode = alfrescoService.getListOfMagCodeGroups(csDataSheetInfo, cpsTicket);
                user.setMagCodes(listOfMagCode);
            }
            if (isS5Customer) {
                final List<String> listOfS5Groups = alfrescoService.getListOfS5Groups(csDataSheetInfo);
                user.setS5CustomerGroups(listOfS5Groups);
            }
            ((Saml2Authentication) authentication).setDetails(user);
            logger.debug("User details are saved in security context");
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

}
