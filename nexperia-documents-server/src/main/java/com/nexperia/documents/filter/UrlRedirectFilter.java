package com.nexperia.documents.filter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class UrlRedirectFilter implements Filter {

    protected final Log logger = LogFactory.getLog(this.getClass());

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        String requestURI = httpServletRequest.getRequestURI();
        String contextPath = httpServletRequest.getContextPath();
        logger.debug("requestURI = " + requestURI);
        logger.debug("contextPath = " + contextPath);
        if (requestURI.equals("/home") && authentication == null) {
            RequestDispatcher dispatcher = servletRequest.getRequestDispatcher("/homePage");
            dispatcher.forward(servletRequest, servletResponse);
            return;
        }
        if (requestURI.equals("/error")) {
            RequestDispatcher dispatcher = servletRequest.getRequestDispatcher("/errorPage");
            dispatcher.forward(servletRequest, servletResponse);
            return;
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
