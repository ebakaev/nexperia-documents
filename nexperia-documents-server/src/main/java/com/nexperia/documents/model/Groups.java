package com.nexperia.documents.model;

import java.util.List;
import java.util.Map;

public class Groups {

    List<Map<String, String>> groups;

    public List<Map<String, String>> getGroups() {
        return groups;
    }

    public void setGroups(List<Map<String, String>> groups) {
        this.groups = groups;
    }
}
