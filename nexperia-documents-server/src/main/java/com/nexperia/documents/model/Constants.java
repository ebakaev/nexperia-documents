package com.nexperia.documents.model;

import java.util.Arrays;
import java.util.List;

public interface Constants {

    String FILE_ID = "FileID";
    String TITLE = "Title";
    String FILE_TYPE = "FileType";
    String FILE_NAME = "FileName";
    String VERSION = "Version";
    String DESCRIPTION = "Description";
    String DESCRIPTIVE_TITLE = "DescriptiveTitle";
    String NODE_UUID = "NodeUUID";
    String PTW = "PublishedOnWeb";
    String MAG_CODES = "MAGCodes";
    String RELEASE_DATE = "ReleaseDate";
    String ADDITIONAL_INFO = "AdditionalInfo";

//    String SPIDER_LIBRARY_DOWNLOAD_FORMAT = "/d/a/workspace/SpacesStore/%s/%s?ticket=%s";
    String DOWNLOAD_FORMAT = "/d/a/workspace/SpacesStore/%s/%s?ticket=%s";
    String TDM_WS_DOWNLOAD_FORMAT = "/s/nexperia/tdm/download?nodeUUID=%s";
    String TDM = "TDM";
    String DPS = "DPS";
    String CS_DATASHEET_GROUP = "GROUP_CS_DataSheet_Users";
    String C5_CUSTOMERS_GROUP = "GROUP__S5_Customers";
//    List<String> MAG_CODE_GROUPS = Arrays.asList("R07", "R71", "R34", "R73", "RE1", "RE2", "RP8", "R33", "R17", "R18", "R01", "R03");
    List<String> _S500_GROUPS = Arrays.asList("_S50", "_S51", "_S54", "_S59");

    List<String> IMAGE_EXTENSIONS = Arrays.asList("jpg","jpeg","gif","png","svg");
}
