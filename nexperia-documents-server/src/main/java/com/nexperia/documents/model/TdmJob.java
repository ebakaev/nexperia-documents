package com.nexperia.documents.model;

public class TdmJob {

    String Description;
    String FileName;
    String FileType;
    String DescriptiveTitle;
    String NodeUUID;
    String Version;
    String MAGCodes;
    String Title;
    String PublishedOnWeb;

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getFileName() {
        return FileName;
    }

    public void setFileName(String fileName) {
        FileName = fileName;
    }

    public String getFileType() {
        return FileType;
    }

    public void setFileType(String fileType) {
        FileType = fileType;
    }

    public String getDescriptiveTitle() {
        return DescriptiveTitle;
    }

    public void setDescriptiveTitle(String descriptiveTitle) {
        DescriptiveTitle = descriptiveTitle;
    }

    public String getNodeUUID() {
        return NodeUUID;
    }

    public void setNodeUUID(String nodeUUID) {
        NodeUUID = nodeUUID;
    }

    public String getVersion() {
        return Version;
    }

    public void setVersion(String version) {
        Version = version;
    }

    public String getMAGCodes() {
        return MAGCodes;
    }

    public void setMAGCodes(String MAGCodes) {
        this.MAGCodes = MAGCodes;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getPublishedOnWeb() {
        return PublishedOnWeb;
    }

    public void setPublishedOnWeb(String publishedOnWeb) {
        PublishedOnWeb = publishedOnWeb;
    }
}
