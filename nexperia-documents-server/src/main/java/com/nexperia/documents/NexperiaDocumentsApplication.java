package com.nexperia.documents;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NexperiaDocumentsApplication {

    public static void main(String[] args) {
        SpringApplication.run(NexperiaDocumentsApplication.class, args);
    }

}
