import {Component, OnDestroy, OnInit} from '@angular/core';
import {INexperiaDocument} from "./core/model/nexperia-document";
import {audit, Subject, takeUntil} from "rxjs";
import {AuthService} from "./core/service/auth.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  public nexperiaDocuments: INexperiaDocument[] | undefined;

  // public isAuthenticated = false;
  private destroySub$ = new Subject<void>();

  constructor(private authService: AuthService) {
  }

  ngOnInit(): void {
      // this.authService.getUserDetails();
    // this.getDocuments();
    // this.authService.isAuthenticated$.pipe(
    //     takeUntil(this.destroySub$)
    // ).subscribe((isAuthenticated: boolean) => this.isAuthenticated = isAuthenticated);
  }

  public ngOnDestroy(): void {
    this.destroySub$.next();
    this.destroySub$.complete();
  }

  // public logout(): void {
  //   this.authService.logout('/').pipe(take(1));
  // }


}
