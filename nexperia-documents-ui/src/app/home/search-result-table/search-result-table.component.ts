import {Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {INexperiaDocument} from "../../core/model/nexperia-document";
import {firstValueFrom, Subject, takeUntil} from "rxjs";
import {ProgressBarService} from "../../core/service/progress-bar.service";
import {SearchResultTableService} from "./search-result-table.service";
import {NzImageService} from "ng-zorro-antd/image";
import {NzNotificationService} from "ng-zorro-antd/notification";
import {IColumnItem} from "./interfaces";
import {DOC_TYPE, DOCUMENT_NAME, IMAGE_EXTENSIONS, MAG_CODES, NAME_TO_FIELD_MAP, PUBLISHED_TO_WEB, SYSTEM, RELEASE_DATE, ADDITIONAL_INFO} from "./constants";
import {NzImagePreviewRef} from "ng-zorro-antd/image/image-preview-ref";
import {NzTableComponent} from "ng-zorro-antd/table/src/table/table.component";
import {NzTableQueryParams} from 'ng-zorro-antd/table';


@Component({
    selector: 'app-search-result-table',
    templateUrl: './search-result-table.component.html',
    styleUrls: ['./search-result-table.component.css']
})
export class SearchResultTableComponent implements OnInit, OnChanges, OnDestroy {
    @ViewChild('table') public table!: NzTableComponent<INexperiaDocument>;

    @Input()
    public dataSource: INexperiaDocument[] = [];

    public checked = false;
    public loading = false;
    public indeterminate = false;
    public listOfCurrentPageData: readonly INexperiaDocument[] = [];
    public checkedIds = new Set<string>();
    public highlightedIds = new Set<string>();
    public listOfColumns: IColumnItem[] = this.searchResultTableService.getListOfColumns();
    public csvData: INexperiaDocument[] = [];

    private destroySub$ = new Subject<void>();
    private queryParams: NzTableQueryParams | undefined;

    constructor(
        private progressBarService: ProgressBarService,
        private searchResultTableService: SearchResultTableService,
        private nzImageService: NzImageService,
        private notification: NzNotificationService,
    ) {
    }

    ngOnInit(): void {
        this.progressBarService.isVisible$.pipe(
            takeUntil(this.destroySub$)
        ).subscribe((loading: boolean) => this.loading = loading);
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['dataSource']) {
            this.listOfColumns.forEach(item => {
                if (item.name === DOCUMENT_NAME) {
                    item.listOfFilter = this.searchResultTableService.getListOfFilterForDocumentNameColumn(this.dataSource);
                }
                if ([PUBLISHED_TO_WEB, SYSTEM, DOC_TYPE, MAG_CODES].includes(item.name)) {
                    const fieldName = NAME_TO_FIELD_MAP[item.name];
                    item.listOfFilter = this.searchResultTableService.getListOfFilterByFieldValue(this.dataSource, fieldName);
                }
            });
        }
    }

    public ngOnDestroy(): void {
        this.destroySub$.next();
        this.destroySub$.complete();
    }

    onCurrentPageDataChange(listOfCurrentPageData: readonly INexperiaDocument[]): void {
        this.listOfCurrentPageData = listOfCurrentPageData;
        this.refreshCheckedStatus();
    }

    onAllChecked(checked: boolean): void {
        this.listOfCurrentPageData
            .forEach(({ name }) => this.updateCheckedSet(name, checked));
        this.refreshCheckedStatus();
    }

    onItemChecked(name: string, checked: boolean): void {
        this.updateCheckedSet(name, checked);
        this.refreshCheckedStatus();
    }

    updateCheckedSet(name: string, checked: boolean): void {
        if (checked) {
            this.checkedIds.add(name);
        } else {
            this.checkedIds.delete(name);
        }
    }

    refreshCheckedStatus(): void {
        const listOfData = this.listOfCurrentPageData;
        if (listOfData?.length) {
            this.checked = listOfData.every(({ name }) => this.checkedIds.has(name));
            this.indeterminate = listOfData.some(({ name }) => this.checkedIds.has(name)) && !this.checked;
        } else {
            this.checked = false;
            this.indeterminate = false;
        }

    }

    onQueryParamsChange(queryParams: NzTableQueryParams) {
        this.queryParams = queryParams;
        // console.log('DMZA', queryParams);
    }

    preview(doc: INexperiaDocument) {
        const src = doc.viewUrl;
        const alt = doc.name;
        if (src) {
            this.highlightedIds.add(doc.name);
            console.log("VIEW src: " + src);
            const ref: NzImagePreviewRef = this.nzImageService.preview([{
                src,
                alt,
            }], {nzZoom: 1, nzRotate: 0});
            firstValueFrom(ref.previewInstance['destroy$']).then(() => {
                this.highlightedIds.delete(doc.name);
            });
        } else {
            this.notification.create(
                'error',
                'Error',
                `Invalid url: ${src}`
            );
        }
    }

    download() {
        if (this.checkedIds.size) {
            this.dataSource
                .filter((doc:INexperiaDocument) => this.checkedIds.has(doc.name))
                .forEach((doc:INexperiaDocument, index: number) => {
                    setTimeout(() => {
                        const link = document.createElement('a');
                        link.href = doc.downloadUrl;
                        link.click();
                        link.remove();
                    }, 1000 * index);
                });
        }
    }

    downloadCSV() {
        const dataForCSV = this.searchResultTableService.getDataPreparedForCSV(this.dataSource, this.queryParams);
        const csvString = this.searchResultTableService.makeCSV(dataForCSV);
        const blob = new Blob([csvString], { type: 'text/csv' });
        const url = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        a.setAttribute('href', url);
        a.setAttribute('download', 'download.csv');
        a.click();
        window.URL.revokeObjectURL(url);
    }

    isImage(doc: INexperiaDocument): boolean {
        const extension = this.searchResultTableService.getFileExtension(doc);
        return IMAGE_EXTENSIONS.indexOf(extension) !== -1;
    }

    needGreenHighlight(columnName: string, doc: INexperiaDocument): boolean {
        return columnName === PUBLISHED_TO_WEB && this.getValueByColumnName(columnName, doc) === 'Yes';
    }

    needRedHighlight(columnName: string, doc: INexperiaDocument): boolean {
        return columnName === PUBLISHED_TO_WEB && this.getValueByColumnName(columnName, doc) === 'No';
    }

    getValueByColumnName(columnName: string, doc: INexperiaDocument): string {
        return doc[NAME_TO_FIELD_MAP[columnName]]
    }

}
