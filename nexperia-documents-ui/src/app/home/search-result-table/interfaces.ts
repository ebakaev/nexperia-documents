import {NzTableFilterFn, NzTableFilterList, NzTableSortFn, NzTableSortOrder} from "ng-zorro-antd/table";
import {INexperiaDocument} from "../../core/model/nexperia-document";

export interface IColumnItem {
    name: string;
    label: string;
    sortOrder: NzTableSortOrder;
    sortFn: NzTableSortFn<INexperiaDocument>;
    sortDirections: NzTableSortOrder[];
    listOfFilter?: NzTableFilterList;
    filterFn?: NzTableFilterFn<INexperiaDocument>;
}
