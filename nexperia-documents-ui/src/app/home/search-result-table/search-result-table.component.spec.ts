import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchResultTableComponent } from './search-result-table.component';
import {NzTableModule} from "ng-zorro-antd/table";
import {NzImageService} from "ng-zorro-antd/image";
import {NzNotificationService} from "ng-zorro-antd/notification";

describe('SearchResultTableComponent', () => {
  let component: SearchResultTableComponent;
  let fixture: ComponentFixture<SearchResultTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [NzTableModule],
      declarations: [ SearchResultTableComponent ],
      providers: [NzImageService, NzNotificationService],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchResultTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
