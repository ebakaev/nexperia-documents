import { TestBed } from '@angular/core/testing';

import { SearchResultTableService } from './search-result-table.service';

describe('SearchResultTableService', () => {
  let service: SearchResultTableService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SearchResultTableService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
