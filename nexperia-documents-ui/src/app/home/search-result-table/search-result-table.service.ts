import {Injectable} from '@angular/core';
import {INexperiaDocument} from "../../core/model/nexperia-document";
import {NzTableFilterFn, NzTableFilterList, NzTableQueryParams} from "ng-zorro-antd/table";
import {IColumnItem} from "./interfaces";
import {
    DESCR_TITLE,
    DESCRIPTION,
    DOC_TYPE,
    DOCUMENT_NAME,
    IMAGE_EXTENSIONS,
    NAME_TO_FIELD_MAP, PUBL_ID,
    PUBLISHED_TO_WEB,
    SYSTEM,
    VERSION,
    MAG_CODES,
    RELEASE_DATE,
    ADDITIONAL_INFO,
    DOWNLOAD_ASSET_URL
} from "./constants";
import {NzTableFilterValue, NzTableSortOrder} from "ng-zorro-antd/table/src/table.types";

@Injectable({
    providedIn: 'root'
})
export class SearchResultTableService {

    constructor() {
    }

    getListOfColumns(): IColumnItem[] {
        const sortDirections = ['ascend', 'descend', null];
        return [
            {
                name: DOCUMENT_NAME,
                label: "Name of a document",
                sortOrder: 'ascend',
                sortFn: this.getSortFn(NAME_TO_FIELD_MAP[DOCUMENT_NAME]),
                sortDirections,
                listOfFilter: [],
                filterFn: (extensions: string[], doc: INexperiaDocument): boolean => {
                    return this.filterFnForDocumentNameColumn(extensions, doc);
                }
            },
            {
                name: DESCR_TITLE,
                label: "Descriptive title of a document",
                sortOrder: null,
                sortFn: this.getSortFn(NAME_TO_FIELD_MAP[DESCR_TITLE]),
                sortDirections,
            },
            {
                name: RELEASE_DATE,
                label: "Last modification date",
                sortOrder: null,
                sortFn: this.getSortFn(NAME_TO_FIELD_MAP[RELEASE_DATE]),
                sortDirections,
            },
            {
                name: DOC_TYPE,
                label: "Document type",
                sortOrder: null,
                sortFn: this.getSortFn(NAME_TO_FIELD_MAP[DOC_TYPE]),
                sortDirections,
                listOfFilter: [],
                filterFn: (filterValues: string[], doc: INexperiaDocument): boolean => {
                    return this.commonFilterFn(filterValues, doc, NAME_TO_FIELD_MAP[DOC_TYPE]);
                }
            },
            {
                name: MAG_CODES,
                label: "Main Article Group",
                sortOrder: null,
                sortFn: this.getSortFn(NAME_TO_FIELD_MAP[MAG_CODES]),
                sortDirections,
                listOfFilter: [],
                filterFn: (filterValues: string[], doc: INexperiaDocument): boolean => {
                    return this.commonFilterFn(filterValues, doc, NAME_TO_FIELD_MAP[MAG_CODES]);
                }
            },
            {
                name: PUBLISHED_TO_WEB,
                label: "Publish to Web",
                sortOrder: null,
                sortFn: this.getSortFn(NAME_TO_FIELD_MAP[PUBLISHED_TO_WEB]),
                sortDirections,
                listOfFilter: [],
                filterFn: (filterValues: string[], doc: INexperiaDocument): boolean => {
                    return this.commonFilterFn(filterValues, doc, NAME_TO_FIELD_MAP[PUBLISHED_TO_WEB]);
                }
            },
            {
                name: PUBL_ID,
                label: "Publication ID",
                sortOrder: null,
                sortFn: this.getSortFn(NAME_TO_FIELD_MAP[PUBL_ID]),
                sortDirections,
            },
            {
                name: SYSTEM,
                label: "In which system document was found",
                sortOrder: null,
                sortFn: this.getSortFn(NAME_TO_FIELD_MAP[SYSTEM]),
                sortDirections,
                listOfFilter: [],
                filterFn: (filterValues: string[], doc: INexperiaDocument): boolean => {
                    return this.commonFilterFn(filterValues, doc, NAME_TO_FIELD_MAP[SYSTEM]);
                }
            },
            {
                name: DOWNLOAD_ASSET_URL,
                label: "Download rendition url",
                sortOrder: null,
                sortFn: this.getSortFn(NAME_TO_FIELD_MAP[DOWNLOAD_ASSET_URL]),
                sortDirections,
            },
            {
                name: ADDITIONAL_INFO,
                label: "Additional Info",
                sortOrder: null,
                sortFn: this.getSortFn(NAME_TO_FIELD_MAP[ADDITIONAL_INFO]),
                sortDirections,
                listOfFilter: [],
                filterFn: (filterValues: string[], doc: INexperiaDocument): boolean => {
                    return this.commonFilterFn(filterValues, doc, NAME_TO_FIELD_MAP[ADDITIONAL_INFO]);
                }
            }
        ]
    }

    getSortFn(fieldName: keyof INexperiaDocument): (a: INexperiaDocument, b: INexperiaDocument) => number {
        return (a: INexperiaDocument, b: INexperiaDocument): number => {
            const itemA = a && a[fieldName] ? a[fieldName] : "";
            const itemB = b && b[fieldName] ? b[fieldName] : "";
            return itemA.localeCompare(itemB);
        }
    }

    filterFnForDocumentNameColumn(extensions: string[], doc: INexperiaDocument): boolean {
        const docExt = this.getFileExtension(doc);
        if (docExt) {
            return extensions.includes(docExt);
        }
        return false;
    }

    commonFilterFn(filterValues: string[], item: INexperiaDocument, itemField: keyof INexperiaDocument): boolean {
        const itemValue = item[itemField];
        return itemValue ? filterValues.includes(itemValue) : false;
    }

    getListOfFilterForDocumentNameColumn(dataSource: INexperiaDocument[]): NzTableFilterList {
        let extension;
        return dataSource.reduce<string[]>((prev: string[], current: INexperiaDocument) => {
            extension = this.getFileExtension(current)
            if (extension && IMAGE_EXTENSIONS.includes(extension) && !prev.includes(extension)) {
                prev.push(extension);
            }
            return prev;
        }, []).map((ext: string) => {
            return {
                text: ext.toUpperCase(),
                value: ext
            }
        });
    }

    getListOfFilterByFieldValue(dataSource: INexperiaDocument[], fieldName: keyof INexperiaDocument): NzTableFilterList {
        const uniqValues = new Set<string>(dataSource.map((item: INexperiaDocument) => item[fieldName]));
        return Array.from(uniqValues).filter((item: string) => !!item).map((item: string) => {
            return {
                text: item,
                value: item
            }
        })
    }

    getFileExtension(doc: INexperiaDocument): string {
        if (doc?.name) {
            let extension = doc.name.split('.').pop();
            if (extension) {
                return extension.toLowerCase();
            }
        }
        return "";
    }

    getDataPreparedForCSV(data: INexperiaDocument[], queryParams: NzTableQueryParams | undefined): INexperiaDocument[] {
        let csvData = [...data];
        if (queryParams) {
            const listOfColumns = this.getListOfColumns();
            const colDefsWithFilterFn = listOfColumns.filter((colDef) => !!colDef.filterFn);
            const colDefsWithSortFn = listOfColumns.filter((colDef) => !!colDef.sortFn);
            let colDef;
            let sortFn;
            let filterFn: NzTableFilterFn<INexperiaDocument>;
            queryParams.filter.forEach((f: {
                key: string;
                value: NzTableFilterValue;
            }, index: number) => {
                if (f.value.length) {
                    colDef = colDefsWithFilterFn[index]
                    if (colDef.filterFn) {
                        filterFn = colDef.filterFn;
                        csvData = csvData.filter((doc) => filterFn(f.value, doc))
                    }
                }
            })
            queryParams.sort.forEach((s: {
                key: string;
                value: NzTableSortOrder;
            }, index: number) => {
                if (s.value) {
                    colDef = colDefsWithSortFn[index]
                    if (colDef.sortFn) {
                        sortFn = colDef.sortFn;
                        csvData = csvData.sort(sortFn)
                        if (s.value === 'descend') {
                            csvData = csvData.reverse();
                        }
                    }
                }
            })
        }
        return csvData;
    }

    makeCSV(data: INexperiaDocument[]): string {
        const listOfColumns = this.getListOfColumns();
        const csvRows = [];

        const headers = listOfColumns.map((colDef) => colDef.name);
        csvRows.push(headers.join(','));

        let row;
        let value: string;
        data.forEach((doc) => {
            row = headers.map((name) => {
                value = doc[NAME_TO_FIELD_MAP[name]] || "";
                value = value.replace('\n', '');
                value = value.replace('"', '""');
                // value = value.replace("'", "''");
                // value = value.replace('\', '\\');
                return value && (value.includes(',') || value.includes(';') || value.includes('\t')) ? `"${value}"` : value;
            }).join(',');
            csvRows.push(row);
        });

        return csvRows.join('\n');
    }
}
