import {INexperiaDocument} from "../../core/model/nexperia-document";

export const DESCR_TITLE = 'Descriptive title';
export const DESCRIPTION = 'Description';
export const DOC_TYPE = 'Document type';
export const DOCUMENT_NAME = 'File name';
export const PUBL_ID = 'Publication ID';
export const PUBLISHED_TO_WEB = 'PtW';
export const SYSTEM = 'System';
export const VERSION = 'Version';
export const MAG_CODES = 'MAG code';
export const RELEASE_DATE = 'Date';
export const ADDITIONAL_INFO = 'Additional Info';
export const DOWNLOAD_ASSET_URL = 'Download URL';


export const NAME_TO_FIELD_MAP: {[key: string]: keyof INexperiaDocument} = {
    [DESCR_TITLE]: 'descriptiveTitle',
    [DESCRIPTION]: "description",
    [DOC_TYPE]: "documentType",
    [DOCUMENT_NAME]: "name",
    [PUBL_ID]: 'publicationId',
    [PUBLISHED_TO_WEB]: "ptw",
    [SYSTEM]: "system",
    [VERSION]: 'version',
    [MAG_CODES]: 'magCodes',
    [RELEASE_DATE]: 'releaseDate',
    [ADDITIONAL_INFO]: 'additionalInfo',
    [DOWNLOAD_ASSET_URL]: 'downloadAssetUrl',
};

export const IMAGE_EXTENSIONS = [
    'jpg',
    'jpeg',
    // 'jpe',
    'gif',
    'png',
    // 'apng',
    'svg',
    // 'svgz',
    // 'bmp',
    // 'dip',
    // 'rle',
    // 'ico',
    // 'webp',
];
