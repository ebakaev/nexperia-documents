import {Component, OnInit} from '@angular/core';
import {INexperiaDocument} from "../core/model/nexperia-document";
import {HttpErrorResponse} from "@angular/common/http";
import {NexperiaDocumentService} from "../core/service/nexperia-document.service";
import {NzNotificationService} from "ng-zorro-antd/notification";
import {AuthService} from "../core/service/auth.service";

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent {

    public nexperiaDocuments: INexperiaDocument[] = [];

    constructor(private notification: NzNotificationService, private nexperiaDocumentService: NexperiaDocumentService, private authService: AuthService) {
    }

    onSearchInputChanged(searchText: string) {
        if (searchText) {
            this.nexperiaDocumentService.find(searchText).subscribe({
                next: (response: INexperiaDocument[]) => {
                    this.nexperiaDocuments = response;
                },
                error: (error: HttpErrorResponse) => {
                    this.createErrorNotification(error.message);
                }
            })
        }
    }

    createErrorNotification(message: string) {
        this.notification.create(
            'error',
            'Error',
            message
        );
    }

}
