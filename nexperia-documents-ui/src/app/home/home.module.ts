import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeComponent} from "./home.component";
import {ToolbarComponent} from "./toolbar/toolbar.component";
import {SearchResultTableComponent} from "./search-result-table/search-result-table.component";
import {SearchInputComponent} from "./search-input/search-input.component";
import {NexperiaDocumentService} from "../core/service/nexperia-document.service";
import {NzFormModule} from "ng-zorro-antd/form";
import {NzInputModule} from "ng-zorro-antd/input";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzDividerModule} from "ng-zorro-antd/divider";
import {NzLayoutModule} from "ng-zorro-antd/layout";
import {RouterModule, Routes} from "@angular/router";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {SearchResultTableService} from "./search-result-table/search-result-table.service";
import {NzIconModule} from "ng-zorro-antd/icon";
import {DownloadOutline, LogoutOutline, MailOutline, SearchOutline, UserOutline} from "@ant-design/icons-angular/icons";
import {NzImageService} from "ng-zorro-antd/image";
import {FeedbackComponent} from './feedback/feedback.component';
import {NzToolTipModule} from "ng-zorro-antd/tooltip";

const routes: Routes = [
    {
        path: '',
        component: HomeComponent
    }
];


@NgModule({
    declarations: [
        HomeComponent,
        ToolbarComponent,
        SearchResultTableComponent,
        SearchInputComponent,
        FeedbackComponent,
    ],
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		RouterModule.forChild(routes),

		NzButtonModule,
		NzDividerModule,
		NzDropDownModule,
		NzFormModule,
		NzIconModule.forChild([LogoutOutline, SearchOutline, UserOutline, DownloadOutline, MailOutline]),
		NzInputModule,
		NzLayoutModule,
		NzTableModule,
		NzToolTipModule,
	],
    providers: [
        NzImageService,
        NexperiaDocumentService,
        SearchResultTableService,
    ],
})
export class HomeModule {
}
