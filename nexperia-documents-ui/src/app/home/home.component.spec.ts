import {ComponentFixture, TestBed} from '@angular/core/testing';

import {HomeComponent} from './home.component';
import {NzNotificationService} from "ng-zorro-antd/notification";
import {OverlayModule} from "@angular/cdk/overlay";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {ToolbarComponent} from "./toolbar/toolbar.component";
import {SearchInputComponent} from "./search-input/search-input.component";
import {SearchResultTableComponent} from "./search-result-table/search-result-table.component";
import {RouterTestingModule} from "@angular/router/testing";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NzLayoutModule} from "ng-zorro-antd/layout";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {NzImageService} from "ng-zorro-antd/image";

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [OverlayModule, HttpClientTestingModule, RouterTestingModule, FormsModule, ReactiveFormsModule, NzLayoutModule, NzDropDownModule],
      declarations: [ToolbarComponent, SearchInputComponent, SearchResultTableComponent, HomeComponent ],
      providers: [NzNotificationService, NzImageService],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
