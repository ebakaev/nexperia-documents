import {AfterViewInit, Component, ElementRef, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
    selector: 'app-search-input',
    templateUrl: './search-input.component.html',
    styleUrls: ['./search-input.component.css']
})
export class SearchInputComponent implements OnInit, AfterViewInit {
    @ViewChild("searchInput") searchInputElRef?: ElementRef;

    @Output()
    public searchInputChanged: EventEmitter<string> = new EventEmitter<string>();

    validateForm!: FormGroup;

    constructor(private fb: FormBuilder) {
    }

    ngOnInit(): void {
        this.validateForm = this.fb.group({
            searchText: [null, [Validators.maxLength(254)]],
        });
    }

    ngAfterViewInit() {
        this.searchInputElRef?.nativeElement.focus();
    }

    submitForm(event: Event) {
        if (this.validateForm.valid) {
            this.searchInputChanged.emit(this.validateForm.value.searchText);
        }
    }

}
