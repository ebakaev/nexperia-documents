import {Component, OnDestroy, OnInit} from '@angular/core';
import {ThemeService} from "../../core/service/theme.service";
import {Subject, take, takeUntil} from "rxjs";
import {AuthService} from "../../core/service/auth.service";
import {ICurrentUser} from "../../core/model/current-user";

@Component({
    selector: 'app-toolbar',
    templateUrl: './toolbar.component.html',
    styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit, OnDestroy {

    public theme = true;
    public currentUser: ICurrentUser | undefined;

    private destroySub$ = new Subject<void>();

    constructor(private authService: AuthService, private themeService: ThemeService) {
    }

    ngOnInit() {
        this.authService.getUserDetails()
            .subscribe(userName =>
            {
                // console.log("Current user is: " + userName);
                this.currentUser = {
                    name: userName
                };
            });
    }

    ngOnDestroy() {
        this.destroySub$.next();
    }

    toggleTheme(): void {
        this.themeService.toggleTheme().then();
    }

}
