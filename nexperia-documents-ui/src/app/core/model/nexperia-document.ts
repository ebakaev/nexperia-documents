export interface INexperiaDocument {
  name: string;
  title: string;
  description: string;
  descriptiveTitle: string;
  documentType: string;
  publicationId: string;
  version: string;
  system: string;
  nodeUUID: string;
  ptw: string;
  downloadUrl: string;
  downloadAssetUrl: string;
  magCodes: string;
  viewUrl: string;
  releaseDate: string;
  additionalInfo: string;
}
