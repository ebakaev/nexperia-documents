import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {INexperiaDocument} from "../model/nexperia-document";
import {environment} from "../../../environments/environment";

@Injectable({providedIn: 'root'})
export class NexperiaDocumentService {
  private apiServerUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient) {
  }

  // public getNexperiaDocuments(): Observable<NexperiaDocument[]> {
  //   return this.http.get<NexperiaDocument[]>(`${this.apiServerUrl}/document/all`);
  // }
  //
  // public addNexperiaDocuments(NexperiaDocument: NexperiaDocument): Observable<NexperiaDocument> {
  //   return this.http.post<NexperiaDocument>(`${this.apiServerUrl}/document/add`, NexperiaDocument);
  // }
  //
  // public updateNexperiaDocuments(NexperiaDocument: NexperiaDocument): Observable<NexperiaDocument> {
  //   return this.http.put<NexperiaDocument>(`${this.apiServerUrl}/document/update`, NexperiaDocument);
  // }
  //
  // public deleteNexperiaDocuments(documentId: number): Observable<void> {
  //   return this.http.delete<void>(`${this.apiServerUrl}/document/delete/${documentId}`);
  // }

  public find(documentName: string): Observable<INexperiaDocument[]> {
    return this.http.get<INexperiaDocument[]>(`${this.apiServerUrl}/document/search/${documentName}`);
  }
}

