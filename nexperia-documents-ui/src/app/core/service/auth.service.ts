import {Injectable, OnDestroy} from '@angular/core';
import {BehaviorSubject, map, Observable, of} from "rxjs";
import {Router} from "@angular/router";
import {ICurrentUser} from "../model/current-user";
import {HttpClient} from "@angular/common/http";
import {environment} from 'src/environments/environment';
import {TokenStorageService} from "./token-storage.service";

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    private apiBaseUrl = environment.apiBaseUrl;
    private user$: BehaviorSubject<ICurrentUser | undefined> = new BehaviorSubject<ICurrentUser | undefined>(undefined);

    constructor(private router: Router, private http: HttpClient, private tokenStorage: TokenStorageService) {
        const isLoggedIn = !!this.tokenStorage.getUser()
        if (isLoggedIn) {
            this.user$.next(this.tokenStorage.getUser());
        }
    }

    public get currentUser$(): Observable<ICurrentUser | undefined> {
        return this.user$.asObservable();
    }

    public getUserDetails(): Observable<string> {
        return this.http.get(`${this.apiBaseUrl}/currentUser`).pipe(
            map((t: any) => {
                // console.log("User details response: " + `${t.name} ${t.firstName} ${t.lastName}`);
                const user = {
                    name: `${t.name} ${t.firstName} ${t.lastName}`
                }
                this.user$.next(user);
                this.tokenStorage.saveUser(user);
                return `${t.name}`;
            })
        );
    }

    public logout(redirect: string): Observable<void> {
        this.user$.next(undefined);
        this.router.navigate([redirect]);
        this.tokenStorage.signOut();
        return of(undefined)
    }
}
