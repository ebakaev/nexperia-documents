import {Injectable, OnDestroy} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ProgressBarService implements OnDestroy {

  private showProgressBar$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor() { }

  public ngOnDestroy(): void {
    this.showProgressBar$.next(false);
    this.showProgressBar$.complete();
  }

  public get isVisible$(): Observable<boolean> {
    return this.showProgressBar$.asObservable();
  }

  show() {
    this.showProgressBar$.next(true);
  }

  hide() {
    this.showProgressBar$.next(false);
  }

}
