export interface ILoginResponse {
    "jwt": string;
    "username": string;
    "firstName": string;
    "lastName": string;
    "email": string;
    "userData": string;
}