import {Injectable} from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor
} from '@angular/common/http';
import {finalize, Observable} from 'rxjs';
import {ProgressBarService} from "../service/progress-bar.service";
import {TokenStorageService} from "../service/token-storage.service";

const TOKEN_HEADER_KEY = 'Authorization';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {

    constructor(
        private progressBarService: ProgressBarService,
        private token: TokenStorageService
    ) {
    }

    intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
        let authReq = request;
        this.progressBarService.show();
        return next.handle(authReq).pipe(finalize(() => this.progressBarService.hide()));
    }
}
